
#!/bin/bash

# flush everything
#echo "" >  playlists/track_playout_history.txt; rm database/show.db ; mk_master_playlist.sh; ./mk_show.py 4 2024-04-17;

rm html/index.html

show_dates=(2024-03-03 2024-03-10 2024-03-17 2024-03-24 2024-03-31 2024-03-31 2024-03-31 2024-03-31 2024-03-31 ) 

counter=0 # set this as input argument to set what episode to start creating from...

episodes=12 # set this as another input argument for how many episodes to produce 

for i in "${show_dates[@]}"
do
    if [i==0]; then

	 echo "" >  playlists/track_playout_history.txt; rm database/show.db ; mk_master_playlist.sh; ./mk_show.py $counter $i ;
	 ((counter++));
    else
	 echo mk_master_playlist.sh;  ./mk_show.py  $counter $i ;
	  ((counter++));
    fi
done
