#!/usr/bin/python3

import pydub, random, os, pathlib

from pydub import AudioSegment
#-------------------------------------
this = "this_is"
broadcasting = "broadcasting_from_"
locations = "broadcast_locations"
out_this_broad_loc = 'out_this_broad_loc'
# ----------------------------------
you = "you_are_listening_to"
sel = "a_selection_of"
by = "brought_to_you_by"
out_you_sel_by = "out_you_sel_by" 
#-------------------------------------

rnd_from = "random_track_from"
label = "the_label"
label_catchline = "label_catchline"
we_hope = "we_hope_you"
# -------------------------------------
uhavebeen = "u_have_been"
sametime = "same_time"
findus = "find_us"

os.makedirs(outdir, exist_ok=True)

def concat3Audio(dir1,dir2,dir3, outdir):

    for x in range(10):
        first =  AudioSegment.from_wav( dir1 + "/" + random.choice(os.listdir(dir1)) )
        second =  AudioSegment.from_wav( dir2 + "/" + random.choice(os.listdir(dir2)) )
        third =  AudioSegment.from_wav( dir3 + "/" + random.choice(os.listdir(dir3)) )    
        out = first\
        .append(second, crossfade=100)\
        .append(third, crossfade=100)
        out.export(outdir + "/" + str(outdir) + "_" + str(x) + ".wav", format="wav")

def concat4Audio(dir1,dir2,dir3,dir4, outdir):

    for x in range(10):
        first =  AudioSegment.from_wav( dir1 + "/" + random.choice(os.listdir(dir1)) )
        second =  AudioSegment.from_wav( dir2 + "/" + random.choice(os.listdir(dir2)) )
        third =  AudioSegment.from_wav( dir3 + "/" + random.choice(os.listdir(dir3)) )
        fourth =  AudioSegment.from_wav( dir4 + "/" + random.choice(os.listdir(dir3)) )    
        out = first\
            .append(second, crossfade=100)\
            .append(third, crossfade=100)\
            .append(third, crossfade=100)
        out.export(outdir + "/" + str(outdir) + "_" + str(x) + ".wav", format="wav")
        
concat3Audio(you, sel, by, out_you_sel_by)











