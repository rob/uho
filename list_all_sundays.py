#!/usr/bin/python3

import datetime
from datetime import date, timedelta

month = 3; #start in march

def allsundays(year):
   d = date(year, month, 1)
   d += timedelta(days = 6 - d.weekday())  # First Sunday
   while d.year == year:
      yield d
      d += timedelta(days = 7)

sundays = []
for d in allsundays(2024):
    sundays.append(d)
    print(d)

print(sundays[4:7]) # output only a slice of the year
