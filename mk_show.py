#!/usr/bin/python3

# CREATE UHO EPISODE -----------------------------------------

import sys, os, datetime, fnmatch, glob, random, time, pathlib, re,\
sqlite3, json, subprocess, uuid, argparse, contextlib
from pydub import AudioSegment
from datetime import timedelta
from os.path import join
from tinytag import TinyTag 
from random import shuffle
from pypika import Query, Table, Field, Column
from PIL import Image, ImageDraw, ImageFont
from jinja2 import Template, Environment, FileSystemLoader
from glob import glob

import tkinter as tk
from tkinter import filedialog as fd 

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--date", help="Show Date")
parser.add_argument("-e", "--episode", help="Episode Number", type=int)
parser.add_argument("-p", "--playlist", help="new, keep or edit", default="keep")
parser.add_argument("-m", "--mp3", action="store_true")
parser.add_argument("-w", "--web", action="store_true")
parser.add_argument("-a", "--art", action="store_true")
parser.add_argument("-s", "--socials", action="store_true")
parser.add_argument("-i", "--insert_fills", action="store_true")
parser.add_argument("-t", "--top_tail", action="store_true")

args = parser.parse_args()

path = pathlib.Path.cwd()

show_name = "UHO"
show_url = "https://uho.rizom.si"
show_rss = "podcast_rss.xml"
show_email = "uho.podcast@gmail.com"
episode_author="Rob Canning"

show_short_description = '''The UhO! podcast presents an eclectic selection of independent music from Slovenia. The show aims to deliver as broad a range of genres as possible; banging techno, sludge, math rock, contemporary classical, doom, free improvisation, noise music, glitch, jazz skronk, field recordings, ambient, drone....etc etc... whatever the genre, you can be sure you are listening to the latest and most innovative music on offer in this part of the world.   Hosted and compiled by Rob Canning, the show is published weekly by Zavod Rizoma and is broadcast in Slovenia on FM by mariborski radio študent: MARŠ. It as also available as a podcast. Use our RSS feed or search for UhO Podcast where ever you subscribe to podcasts'''

episode_number = args.episode
input_date = args.date
episode_date = datetime.datetime.now().strftime("%Y-%m-%d")
episode_duration = 0
show_cover = ""
archive = []
#artists_played = []
artist_abreviated = []

episode_artists  = []

# sqlite database connection
# TODO make this a input vaiable for testing db and live db
conn = sqlite3.connect("database/uho_music.db")

web_path = "{0}/html/episode/{1}/img".format(path, episode_number)
if not os.path.exists(web_path):
    os.makedirs(web_path)

# /////////////////////////////////////////////////

def set_episode_date(input_date):
    # second CLI argument is showdate in  YYYY-MM-DD 
    global episode_date
    d = input_date.split("-")
    episode_date =  datetime.date(int(d[0]),int(d[1]),int(d[2])).strftime("%A, %d %B, %Y")
    return episode_date

def get_intro():
    intropath = "audio/intros"
    intro = random.choice(os.listdir(intropath))
    #final_playlist.insert(0, str(os.path.abspath(intropath)) + "/"  + str(intro))
    return str(str(intropath) + "/" + str(intro))

def get_midshow_CTA():
    midshow_path = "audio/midshow_CTA"
    midshow_CTA = random.choice(os.listdir(midshow_path))
    #final_playlist.insert(0, str(os.path.abspath(intropath)) + "/"  + str(intro))
    return str(str(midshow_path) + "/" + str(midshow_CTA))

def select_specific_track(conn, episode_number, episode_duration):
    filename = fd.askopenfilename()
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM MUSIC_LIBRARY WHERE path = ? ", (filename, ))
    r = cursor.fetchone()
    print(filename)
    return r

def choose_a_track(conn, episode_number, choice ):
    # random or specific file depending on value of "choice" argument
    print("adding a track")
    global episode_duration
    global track_count

    set_episode_date(input_date)

    max_track_dur = 10
    min_track_dur = 2

    cursor = conn.cursor()

    if choice == 0: # randomly select from database
        cursor.execute("SELECT *  FROM MUSIC_LIBRARY ORDER BY RANDOM() LIMIT 1 ;")
        r = cursor.fetchone() # FETCH ONE RANDOM TRACK FROM THE DATABASE
        print(r)
    else: # select a specific file from db via GUI
         r = select_specific_track(conn, episode_number, episode_duration)

    song =             str(r[9])
    track_label =      str(r[1])
    track_album  =     str(r[2])
    track_title =      str(r[3])
    track_artist =     str(r[4])
    track_duration =   float(r[6])
    track_genre =      str(r[5])
    track_year =       str(r[7])
    track_path = song #'/'.join(song.split('/')[0:-1])
    track_comment =    str(r[11])

    print(track_duration/60)

    # SOME LOGIC TO SEE IF WE ALLOW THAT TRACK OR NOT
    if  check_album(track_album) is True \
        and check_artist(track_artist) is True \
        and check_for_KM_compilation(track_album) is True:
        if  check_archive(track_path) is True:
      
            if  track_duration > min_track_dur * 60:  
                
                if  int(track_duration) < max_track_dur * 60:  
                    # TODO put in a function
                    #art =  string=re.sub("\(.*?\)","",track_artist)
                    # shorten verbose artist names such as trojnik Trojnik (Cene Resnik, Tomaž Grom, Vid Drašler) 
                    #art =  string=re.sub("and","&",art)
                    #artist_abreviated.append(art)
                    #print(artist_abreviated)
                    if not track_year: # where missing metadata give a dummy value
                        track_year = "0000" # TODO fixme
                       
                    e =  Table('EPISODES')
                    track_number =  '{:0>2}'.format(track_count)
                    id = str(uuid.uuid4())
                    print("adding..." + str(track_count))
                    q = e.insert(id, episode_number,  track_number, str(episode_date),\
                                 track_album, track_title, track_artist, \
                                 track_duration,track_genre, track_year, \
                                 track_path, track_label, track_comment, "","","")
                    cursor.execute(str(q))
                    conn.commit()                      
                    track_count += 1;
                    episode_duration = episode_duration + track_duration 

    return '{:0>2}'.format(track_count)

# TODO figure out logging to see what is getting rejected etc.

#        else: print("TRACK TOO SHORT..........." )
          #     else: print("TRACK TOO LONG..........." )
         #  else: print("SONG PLAYED IN PREVIOUS EPISODE" )
        #else: print("ARTIST ALREADY IN PODCAST")

    
def check_archive(track_path):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM EPISODES WHERE path = ?", (track_path,))
    data=cursor.fetchone()

    if data is None:
        return True # no previous play record found... the track can be added
    else:
        print('Track has been played on a previous episode', [track_path, data[0]]) 
        return False

#TODO limit number of KM Compilations in new function

def check_for_KM_compilation(track_album):
    "TODO fix me"
    # check if album is played already on this episode - maybe last 2 episodes >= or similar
    cursor = conn.cursor()
    
    q = 'SELECT * FROM EPISODES WHERE album = "{0}"  AND EPISODE = {1} '.format(track_album,episode_number, "lubski" )    
    #q = "SELECT album FROM EPISODES WHERE   instr(album, 'Klubski')  AND episode = '{1}' ".format(track_album,episode_number )    
    cursor.execute(q)
    data=cursor.fetchone()

    if data is  None:
        return True # no previous play record found... the track can be added3
    else:
        print('Klubski Maraton compilation already featured  this episode!?', [track_album, data]) 
        return False


def check_album(track_album):
    # check if album is played already on this episode - maybe last 2 episodes >= or similar
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM EPISODES WHERE album = ? AND episode = ?", (track_album,episode_number))
    data=cursor.fetchone()
    #print(data)
    
    if data is None:
        return True # no previous play record found... the track can be added
    else:
        print('Album already featured on this episode ? ?', [track_album, data[0]]) 
        return False

    
def check_artist(track_artist):

    # check if artist is played already on this episode - maybe last 2 episodes >= or similar
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM EPISODES WHERE artist = ? AND episode = ?", (track_artist,episode_number))
    data=cursor.fetchone()

    if data is None:
        return True # no previous play record found... the artist can be added
    else:
        print('ARTIST already featured on this episode:', [track_artist, data[0]]) 
        return False

# -----------------------------------------------------------------------------

track_count = 1

def create_episode_playlist(conn, episode_number):
    global episode_duration
    global archive
    global track_count
    track_count = 1

    max_track_dur = 14
    min_track_dur = 2
    episode_duration = 0

    set_episode_date(input_date)
    
    # first clear the sqlite table rows for the episode
    cursor = conn.cursor()
    cursor.execute('DELETE FROM EPISODES WHERE episode = {0}'.format(episode_number))
       
    # TODO what is most important 12 tracks or 60 minutes
    # 56 mins of audio rest for fillers - aprox.
    while episode_duration  < 56 * 60 and track_count <= 12 :
        print(track_count)
        choose_a_track(conn, episode_number, 0)

    modify_playlist(conn,  episode_number, episode_duration)


def playlist_replace_track(conn, episode_number, episode_duration):

    global track_count
    cursor = conn.cursor()            
    track_to_replace = int(input(">>>>>>>>>> Which track number would you like to REPLACE? : "))
    specific_or_random = int(input(">>>>>>>>>> with a specific (1) or random track (0) : "))

    # delete the old track
    cursor.execute('DELETE FROM EPISODES WHERE episode = {0} AND track_number={1}'\
                   .format(episode_number, track_to_replace ))
    conn.commit()
    # this is where the index number comes from in the below choose_a_track function
    track_count = track_to_replace  
    # insert the new track
    choose_a_track(conn, episode_number, specific_or_random)
    conn.commit()
    # get the new episode time
    cursor.execute('SELECT SUM(trackdur) FROM EPISODES WHERE EPISODE=? ', [episode_number])
    episode_duration = cursor.fetchone()[0]
    modify_playlist(conn,  episode_number, episode_duration)


def playlist_add_track(conn, episode_number, episode_duration):
    global track_count
    cursor = conn.cursor()            
    #track_to_add = int(input(">>>>>>>>>> Where would you like to ADD the track? : "))
    
    # this is where the index number comes from in the below choose_a_track function
    #track_count = track_to_add 
    # add the new track

    # TODO specific track via gui or random choice or random from gui selected directory
    # choose a random
    choose_a_track(conn, episode_number)   

    # re sequence track numbers
    cursor.execute('\
    UPDATE EPISODES SET track_number = (SELECT COUNT(*)+1 FROM EPISODES r \
    WHERE EPISODES.track_number>r.track_number AND EPISODE=?);',[episode_number] )
    conn.commit()

    # get the new episode time
    cursor.execute('SELECT SUM(trackdur) FROM EPISODES WHERE EPISODE=? ', [episode_number])
    episode_duration = cursor.fetchone()[0]
    
    modify_playlist(conn,  episode_number, episode_duration)


def playlist_delete_track(conn, episode_number, episode_duration):

    global track_count
    cursor = conn.cursor()            
    track_to_replace = int(input(">>>>>> Which track number would you like to DELETE? : "))
    # delete the old track
    cursor.execute('DELETE FROM EPISODES WHERE episode = {0} AND track_number={1}'\
                   .format(episode_number, track_to_replace ))
    conn.commit()
    
    # interate through the column an insert new track_number sequence to replace the gap
    cursor.execute('\
    UPDATE EPISODES SET track_number = (SELECT COUNT(*)+1 FROM EPISODES r \
    WHERE EPISODES.track_number>r.track_number AND EPISODE=?);',[episode_number] )
    conn.commit()

    # get the new episode time
    cursor.execute('SELECT SUM(trackdur) FROM EPISODES WHERE EPISODE=? ', [episode_number])
    episode_duration = cursor.fetchone()[0]
    
    modify_playlist(conn,  episode_number, episode_duration)


def playlist_shuffle_tracks(conn, episode_number, episode_duration):
    os.system("clear")
    # shuffle the episode
    cursor = conn.cursor() 
    q = 'SELECT * FROM EPISODES  WHERE EPISODE = {0} ORDER BY random();'.format(episode_number)
    cursor.execute(q)
    r = cursor.fetchall()
    # delete the old episode
    cursor.execute('DELETE FROM EPISODES WHERE episode = {0}'.format(episode_number))
    track_count = 1;
    # write in the shuffled version
    print("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    print(">>NEW SHUFFLED PLAYLIST: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n")
    for i in r:
        print('{0} :  {1} {2} {3} {4} '.format(i[2], i[4], i[5], i[6], i[7],))
        e =  Table('EPISODES')
        track_number = '{:0>2}'.format(track_count)
        id = str(uuid.uuid4())
        q = e.insert(id, episode_number,  track_number, str(episode_date),\
                     i[4], i[5], i[6],  i[7], i[8], i[9], i[10], i[11], i[11], "","","") #TODO fix last index
        cursor.execute(str(q))
        track_count += 1;
        episode_duration = episode_duration  
    conn.commit()                      
    print("\n>>>>>>>>>>>>>>TOTAL EPISODE DURATION:>>>>    {0}       >>>>>>>>>>>>>>>>>>>>>>>\n"\
          .format(timedelta(seconds=round(episode_duration))) )

    modify_playlist(conn,  episode_number, episode_duration)

    
def playlist_preview_track(conn, episode_number, episode_duration):
    cursor = conn.cursor() 
    # preview a track --------------------------------------
    preview_track = input(">>>>>>>> Which track would you like to preview : ")
    cursor.execute('SELECT path FROM EPISODES WHERE EPISODE=? AND track_number=?', [episode_number, preview_track ])        
    preview_track_path = cursor.fetchone()[0]
    print(preview_track_path)
    subprocess.Popen(["vlc '{0}' &".format(preview_track_path)], shell=True)
    modify_playlist(conn,  episode_number, episode_duration)
    

def modify_playlist(conn,  episode_number, episode_duration):
    cursor = conn.cursor()    
    cursor.execute('SELECT * FROM EPISODES WHERE EPISODE=? ORDER BY track_number ASC', [episode_number])
    preview = cursor.fetchall()
    cursor.execute('SELECT SUM(trackdur) FROM EPISODES WHERE EPISODE=? ', [episode_number])
    episode_duration = cursor.fetchone()[0]

    os.system("clear")
    print("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    print(">> PROPOSED EPISODE #{0} PLAYLIST: ({1}) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"\
          .format(episode_number, timedelta(seconds=round(episode_duration))))
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n")

    for i in preview:
        print('| {0} |  {3} ||| {2} ||| {1} ||| {5} ||| [{6}] ||| {4} |||\n'\
              .format('{:0>2}'.format(i[2]), i[4], i[5], i[6],\
                      timedelta(seconds=round(i[7])), i[9], i[11]  ) )
        
    print(">> SELECT AN OPTION: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    
    user_input = input('''
                             (l)isten to track
                             (n)EW (playlist),
                             (s)HUFFLE
                             (r)EPLACE random
                             (c) choose specific track
                             (d)ELETE
                             (a)DD 

>>>>>>>>>>>>>>>>>>>>>>>>>>> OR PRESS ENTER TO PROCEED................... : ''')
    
    if user_input == 'n':
        track_count = 1
        create_episode_playlist(conn,  episode_number)
        
    elif user_input == 's':
        playlist_shuffle_tracks(conn, episode_number, episode_duration)

    elif user_input == 'r':
        playlist_replace_track(conn, episode_number, episode_duration)

    elif user_input == 'c':
        select_specific_track(conn, episode_number, episode_duration)

    elif user_input == 'l':
        playlist_preview_track(conn, episode_number, episode_duration)

    elif user_input == 'd':
        playlist_delete_track(conn, episode_number, episode_duration)

    elif user_input == 'a':
        playlist_add_track(conn, episode_number, episode_duration)

    else:
        print(">>>>>>>>>>>>>>>>>> Goodbye Happy DJ   !!!!!!!!!!!!!!")
        episode_duration = timedelta(seconds=round(episode_duration))
        print("Total Duration = {0}".format(episode_duration))

# ----------------------------------------------------------------------
# image processing section
# ----------------------------------------------------------------------

def combine_images(columns, space, images, variants:int):
    global show_cover
    print(images)
    set_episode_date(input_date)
    rows = len(images) // columns

    if len(images) % columns:
        rows += 1

    width_max = 500 #max([Image.open(image).width for image in images])
    height_max = 500# max([Image.open(image).height for image in images])
    background_width = width_max*columns + (space*columns)-space
    background_height = height_max*rows + (space*rows)-space
    #background = Image.new('RGBA', (background_width, background_height), (0, 0, 0, 255))
    background = Image.new('RGB', (width_max*columns , height_max*columns), (0, 0, 0, 255))
    
    x = 0
    y = 0

    for i, image in enumerate(images):
        imga = Image.open(image)
        size = (500,500)
        img = imga.resize(size)
        #img = img.rotate(random.randrange(360))
        x_offset = int((width_max-img.width)/2)
        y_offset = int((height_max-img.height)/2)
        background.paste(img, (x+x_offset, y+y_offset+100))
        x += width_max + space
        if (i+1) % columns == 0:
            y += height_max + space
            x = 0

        im = ImageDraw.Draw(background)
        mf_h1 = ImageFont.truetype('fonts/Antonio-Light.ttf', 280)
        mf_h2 = ImageFont.truetype('fonts/Antonio-Light.ttf', 65)
        mf_h3 = ImageFont.truetype('fonts/Antonio-Regular.ttf', 50)
        mf_h4 = ImageFont.truetype('fonts/Antonio-Light.ttf', 50)
           
        h2_spc = 85
        h2_baseline = 1530
      
    # Add Text to the image ----------------------------------------

    # some logic to shuffle the list if sub sections of list are too long for layout
    str_length_thresh = 55
    artist_abreviated = list_episode_artists()

    #TODO  if an artist is listed as Various Arist then remove it from cover display and think of logic
    #TODO exit while loop if it cant find a solution and restart script or shrink font and adjust rules
    while \
       [len(s) for s in [''.join(artist_abreviated[0:3])]][0] > str_length_thresh   or\
       [len(s) for s in [''.join(artist_abreviated[3:6])]][0] > str_length_thresh  or\
       [len(s) for s in [''.join(artist_abreviated[6:9])]][0] > str_length_thresh  or \
       [len(s) for s in [''.join(artist_abreviated[9:12])]][0] > str_length_thresh:
        print('''one of the lines is longer than fits the page...
        ...................shuffling the list for a better look: {0}'''.format(artist_abreviated))

        random.shuffle(artist_abreviated)


    im.text((30,10), '''An eclectic selection of independent and alternative music from Slovenia: {0}
    '''\
            .format(episode_date,episode_number), fill="white", font=mf_h3)
    im.text((30,280), ''' THIS WEEK ON \n EPISODE #{0} of \n {1}!'''.upper()\
            .format(episode_number, show_name), fill="white", font=mf_h1, stroke_width=2, stroke_fill='black')
    im.text((30, h2_baseline + (h2_spc*1) ),  '''m u s i c _ f r o m _ :  {0}'''\
            .format(' | '.join(artist_abreviated[0:3])), (255,255,255), font=mf_h2)
    im.text((30, h2_baseline + (h2_spc*2) ), "{0}"\
            .format(' | '.join(artist_abreviated[3:6])), (255,255,255), font=mf_h2)
    im.text((30, h2_baseline + (h2_spc*3)), "{0}"\
            .format(' | '.join(artist_abreviated[6:9])), (255,255,255), font=mf_h2)
    im.text((30, h2_baseline + (h2_spc*4)), "{0}"\
            .format(' | '.join(artist_abreviated[9:12])), (255,255,255), font=mf_h2)

    im.text((1540,1888), '''{0} '''\
            .format(show_url, show_name, episode_date,episode_number), fill="white", font=mf_h4)
  
    show_cover = 'img/cover{2}.jpg'.format(episode_number,episode_date, variants)
    background.convert("P", palette=Image.ADAPTIVE, colors=256)
    background.save("html/" + "episode/{0}/{1}".format(episode_number, show_cover), optimize=True)
    #convert -delay 100 -loop 0 html/episode/2/img/show_cover_2024-01-12* animatedGIF.gif
    return show_cover


# -------------------------------------------------------------------------------

def create_show_coverart(variants):
    set_episode_date(input_date)
    
    final_playlist = []

    cursor = conn.cursor()    
    cursor.execute('SELECT path FROM EPISODES WHERE EPISODE=? ORDER BY track_number ASC', [episode_number])
    preview = cursor.fetchall()
    for i in preview: # database >> python list
        final_playlist.append(i[0]) 

    
    show_cover_jpgs = []
    # in the directory containing songs find jpg and pngs containing string "cover"
    for dir in final_playlist:
        path = pathlib.Path(dir).parent
        for file in os.listdir(path):
            for p in [".png", ".jpg", ".jpeg"]:
                if p in file:            
                    for i in ["cover", "COVER"]:
                        if i in file:
                            show_cover_jpgs.append(str(path) + "/" + file)

    print(''''\n  ------------------------
    creating show cover art
    ------------------------ ''')
  
    #   if len(show_cover_jpgs) > 0: # duplicate this for variations of geometry 
    for i in range(variants):
        x = show_cover_jpgs[:12]
        print(x)
        combine_images(columns=4, space=10, images=random.sample(x,len(x)),variants=i)        
    return show_cover

def list_episode_artists():
    episode_artists = []
    cursor = conn.cursor()
    cursor.execute('SELECT artist FROM EPISODES WHERE EPISODE=?', [episode_number])
    a = cursor.fetchall()
    for i in a: # database >> python list
        artist =  string=re.sub("\(.*?\)","",i[0])
        # shorten verbose artist names such as trojnik Trojnik (Cene Resnik, Tomaž Grom, Vid Drašler)
        #artist=(','.join(artist))

        #artist = string=re.sub('\[',"",artist)
        #artist = string=re.sub(']',"",artist)
        #artist = string=re.sub('\'',"",artist)
        #artist = string=re.sub("and","&",artist)
        #artist = string=re.sub("and","&",artist)

        episode_artists.append(artist)
    #print(episode_artists)
    return episode_artists

def social_media_post():
    a = list_episode_artists()
    n=(', '.join(a))
    #a = (a.replace(" ,", ""))
    heads = (
        "This week Uho! brings you music from",
        "On this weeks episode of Uho! we have music from",
        "This week on Uho! you will hear music from",
        "Some fine tunes this week on Uho from",
        "This week Uho! will delight your ears with tracks from")

    tags = "#independentmusic #alternativemusic #experimentalmusic #improvisedmusic #podcast #slovenianmusic #slovenia #musicpodcast #glasba"

    labels = '''@zavodsploh #zavodsploh @kaparecords @moonleerecords @naturescene_music @kamizdat #pharmafabrik @radiostudent
    FM broadcast on @radiomars 95.9 in Maribor SI podcast hosted by @retroerto a @zavodrizoma production thanks to @ljudmila for the podcast hosting'''

    with open("html/episode/{0}/socials.html".format(episode_number), "w") as episode_page:
            episode_page.write('''
            <html lang="en">
            <head>
            <title>{{show_name}}</title>
            <meta charset="utf-8">
            </head>

            <body>

            <h2>TEXTS FOR SOCIAL MEDIA POSTS BY THE UHO_BOT!</h2>

            <h3>FB and Insta!</h3>
     ''' )

    for i in range(len(heads)):

        p = '''{0} {1} and {2} {3} {4}'''\
        .format(random.choice(heads), str(a[:-1]).upper(),\
                str(a[len(a)-1]).upper(), tags, labels)
        p = (p.replace("'", ""))
        p = (p.replace("", ""))
        p = (p.replace("[", ""))
        p = (p.replace("]", ""))
        p = (p.replace(" ,", ","))

        print(p + "charachter count: _____________" + str(len(str(p))))

        with open("html/episode/{0}/socials.html".format(episode_number), "a") as episode_page:
            episode_page.write("<p>{0}</p> <br><br>".format(p))

        twitter_end  = "http://uho.rizom.si"

    with open("html/episode/{0}/socials.html".format(episode_number), "a") as episode_page:
        episode_page.write("<h3>Twtter X 280 char limit</h3>".format(p))

    for i in range(len(heads)):

        p = '''uho! episode #{0} now available music from: {1} and {2} {3}'''\
            .format(episode_number, str(a[:-1]).upper(),\
                    str(a[len(a)-1]).upper(), twitter_end )
        p = (p.replace("'", ""))
        p = (p.replace("", ""))
        p = (p.replace("[", ""))
        p = (p.replace("]", ""))
        p = (p.replace(" ,", ","))

        print(p + "character count: _____________" + str(len(str(p))))

    with open("html/episode/{0}/socials.html".format(episode_number), "a") as episode_page:
        episode_page.write("<p>{0}</p> <br><br>".format(p))

    with open("html/episode/{0}/socials.html".format(episode_number), "a") as episode_page:
        episode_page.write('''
        </body>
        ''')


def create_animated_gif():
    # filepaths
    fp_in = "/home/rob/uho/html/episode/2/img/*.jpg".format(episode_number)
    fp_out = "/home/rob/uho/html/episode/2/img/show_cover.gif"

    # use exit stack to automatically close opened images
    with contextlib.ExitStack() as stack:

        # lazily load images
        imgs = (stack.enter_context(Image.open(f))
                for f in sorted(glob.glob(fp_in)))

        # extract  first image from iterator
        img = next(imgs)

        # https://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html#gif
        img.save(fp=fp_out, format='GIF', append_images=imgs,
                 save_all=True, duration=200, loop=0)

        
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

def create_podcast():
#TODO normalise-audio the wav fills before conversion to flac then replay-gain
    print('''------------------------
    creating show audio
    ------------------------''')
    archive_path = "archive/e".format(episode_number)

    if not os.path.exists(archive_path):
        os.makedirs(archive_path)

    #from distutils.dir_util import copy_tree
    #copy_tree("archive/blank_fills/", "archive/e/{0}/audio_fills".format(episode_number))

        
    fill_path = "{0}/{1}/audio_fills".format(archive_path, episode_number)
    # grab the conpleted playlist from the database
    final_playlist = []
    cursor = conn.cursor()    
    cursor.execute('SELECT * FROM EPISODES WHERE EPISODE=? ORDER BY track_number ASC', [episode_number])
    preview = cursor.fetchall()

    #TODO add track duration counter into this loop to get final duration
    # metaflac --show-total-samples --show-sample-rate foo.flac | tr '\n' ' ' | awk '{print $1/$2}' -

    # -----------------------------------------------------------------------
    # ------------ MAIN SHOW STRUCTURE STARTS HERE --------------------------

    for i in preview: # database >> python list
        # if its the first track then put the show intro before it
        # no track intro but yes track outro

        if i[2] == 1: 
            intro = get_intro() 
            final_playlist.insert(0,intro) # insert the intro <---------------
            final_playlist.insert(1, i[10]) # insert the first track (crossfade happens later)
            final_playlist.append("{0}/{2}_out.flac".format(fill_path, episode_number, i[2] ))

        elif  i[2] == len(preview):
            # if it's the last track then dont have an outro <-----------------
            final_playlist.append("{0}/{2}_in.flac".format(fill_path, episode_number, i[2] )) 
            final_playlist.append(i[10]) 
        else:
            # put the intros and outros to rest of tracks <--------------------
            final_playlist.append("{0}/{2}_in.flac".format(fill_path, episode_number, i[2]  )) 
            final_playlist.append(i[10]) 
            final_playlist.append("{0}/{2}_out.flac".format(fill_path,  episode_number, i[2] )) 
    
    # Insert a midshow CTA between track 6 outro and track 7 intro --------
    
    final_playlist.insert(24, get_midshow_CTA()) # <-----------------------
    
    # ------------ MAIN SHOW STRUCTURE ENDS HERE --------------------------
    
    print('''
    ========================================================================
    {0}
    ========================================================================'''.format(final_playlist))
    c = 0
    for i in final_playlist:
        c += 1
        print(c)
        print(i)
    
    playlist_length = len(final_playlist) / (1000*60)
    # -----------------------------------------------------------------------

    # COVERT LIST TO AUDIO SEGMENTS AND DO THE CROSSFADES AND RENDER OUTPUT ETC

    show_playlist = [AudioSegment.from_file(flac_file) for flac_file in final_playlist]

    # FADE IN THE FIRST TRACK

    track1 = show_playlist[1].fade_in(18000) # 18 second fade in to track 1

    # OVERLAY THE FADING IN FIRST TRACK WITH THE SHOW INTRO

    playlist = track1.overlay(show_playlist[0],position= (len(track1)*-1)+50, gain_during_overlay=0)

    # JOIN IT ALL TOGETHER  - NEED TO BREAK THIS UP IF ANY MORE CROSS FADES NEEDED
    
    for track in show_playlist[2:]: # add the rest of the tracks in playlist
        print(track)
        playlist = playlist.append(track)

    # TODO CLI flag to generate FLAC        
    # save the entire poidcast as FLAC
    with open("html/episode/{0}/show.flac".format(episode_number), 'wb') as out_f:
        print("FLAC output file opened...writing to file...")
        playlist.export(out_f, format='flac')
        print("FLAC audio file exported...")

    # save the entire poidcast as MP3

    with open("html/episode/{0}/show.mp3".format(episode_number), 'wb') as out_f:
        print("MP3 output file opened...writing to file...")
        playlist.export(out_f, format='mp3',\
                        tags={'title': str('Uho! Episode #{0} '.format(episode_number)),\
                              'artist': 'https://uho.rizom.si',\
                              'album': 'The Podcast UHO!',\
                              'comments': 'https://uho.rizom.si'})

        print("MP3 audio file exported...")

    # TODO duration of final file
    # ffprobe -i html/episode/0/show.mp3 -show_entries format=duration -v quiet -of csv="p=0" -sexagesimal
        
### ------------------------------------------------------------        

def create_html_homepage_from_template():
    # TODO "on this weeks show" variants fed to html from list here

    set_episode_date(input_date)
    
    env = Environment(loader=FileSystemLoader('html/templates'))
    homepage_template = env.get_template('homepage.jinja')

    conn = sqlite3.connect("database/uho_music.db")
    cursor = conn.cursor()
    
    #global episode_artists

    #TODO fix this - now it reports zero
    # get number of episodes from DB
    cursor.execute('SELECT MAX(episode) FROM EPISODES')

    num_eps = cursor.fetchone()[0]
    num_eps = num_eps
    
    for i in range(num_eps):
        artists = []
        cursor.execute("SELECT artist FROM EPISODES WHERE episode=? ORDER BY track_number ASC", [i])
        rows = cursor.fetchall()

        for artist in rows:
            art =  string=re.sub("\(.*?\)","", artist[0])
            # shorten verbose artist names such as trojnik Trojnik (Cene Resnik, Tomaž Grom, Vid Drašler) 
            artist =  string=re.sub("and","+",art)
            artists.append(artist)
        episode_artists.append(artists)

    episodes = []
    for i in range(num_eps):      # get this from new table EPISODE_STATS number of tracks in DB
        #print(episode_artists[i])
        an_episode = dict(date="2012-02-", \
                          episode_artists=str(episode_artists[i])\
                          .strip("[").strip("]").strip("\'"), episode_number=i, \
                          episode_date=episode_date, \
                          episode_duration=episode_duration)

        episodes.append(an_episode)
    # reversed order to most recent episode appears first in list
    episodes = reversed(episodes[0:episode_number])

    cursor = conn.cursor()
    cursor.execute('SELECT * FROM EPISODES WHERE episode=? ORDER BY track_number ASC', [episode_number]) 
    r = cursor.fetchall()
    
    for t in r:
        song =             str(t[0])
        #track_label =      str(t[1])
        track_number  =     str(t[2])
        track_album  =     str(t[3])
        track_title =      str(t[4])
        track_artist =     str(t[5])
        track_duration =   float(t[7])
        track_genre =      str(t[8])
        track_year =       str(t[9])
        
    #TODO remove single quptes not working in episode_artists
    # populate the jinja html template for the EPISODE
    #''.join(random.choice((str.upper,str.lower))(x) for x in show_name)
    output_from_parsed_template = homepage_template.render(\
                                    show_name=show_name, show_url=show_url, show_rss=show_rss, \
                                    episodes=episodes, episode_author=episode_author,\
                                    episode_date=episode_date,\
                                    episode_duration=episode_duration, episode_number=episode_number, \
                                    episode_artists=str(episode_artists[episode_number])\
                                    .strip("[").strip("]").strip('\'').strip('\''), \
                                    about_show=show_short_description, \
                                    episode_image="img/cover0.jpg".format(episode_number))

    with open("html/index.html".format(episode_number), "w") as episode_page:
        episode_page.write(output_from_parsed_template)


def parse_db_episode():
    playlist_table = []
    env = Environment(loader=FileSystemLoader('html/templates'))
    episode_template = env.get_template('episode.jinja')

    cursor = conn.cursor()

    e = Table('episodes')
    q = Query.from_(e).select(
        e.episode
    ).where(
        e.episode ==  episode_number)

    cursor.execute('SELECT * FROM EPISODES WHERE EPISODE=? ORDER BY track_number ASC', [episode_number])
    #cursor.execute(str(q))
    
    r = cursor.fetchall()
    for t in r:
        song =             str(t[0])
        track_number  =     str(t[2])
        track_album  =     str(t[4])
        track_title =      str(t[5])
        track_artist =     str(t[6]).upper()
        track_duration =   float(t[7])
        track_genre =      str(t[8])
        track_year =       str(t[9])
        track_label =      str(t[11])
        track_comment =  str(t[12])
        
        playlist_entry = [track_number, track_title, track_artist,\
                          track_album, track_year,\
                          timedelta(seconds=round(track_duration)),\
                          track_label.upper() ]

        playlist_table.append(playlist_entry)

    # populate the jinja html template for the EPISODE

    output_from_parsed_template = episode_template.render(\
                                    episode_artists=list_episode_artists(),\
                                    show_name=show_name,\
                                    episode_author=episode_author,\
                                    episode_number=episode_number, show_url=show_url, \
                                    show_rss=show_rss,\
                                    episode_duration=timedelta(seconds=round(episode_duration)),\
                                    episode_date=episode_date, about_show=show_short_description,\
                                    episode_playlist=playlist_table,\
                                    episode_image="img/cover0.jpg".format(episode_number))
    return output_from_parsed_template
        
def create_html_episode_from_template(episode_number, episode_duration):
    set_episode_date(input_date)
    
    output_from_parsed_template = parse_db_episode()

    
    # playlist_table = []

    # # maybe a jinja2 template loop here instead 
    # cursor = conn.cursor()

    # e = Table('episodes')
    # q = Query.from_(e).select(
    #     e.episode
    # ).where(
    #     e.episode ==  episode_number)

    # cursor.execute('SELECT * FROM EPISODES WHERE EPISODE=?', [episode_number])
    # #cursor.execute(str(q))
    
    # r = cursor.fetchall()
    # for t in r:
    #     song =             str(t[0])
    #     track_number  =     str(t[2])
    #     track_album  =     str(t[4])
    #     track_title =      str(t[5])
    #     track_artist =     str(t[6]).upper()
    #     track_duration =   float(t[7])
    #     track_genre =      str(t[8])
    #     track_year =       str(t[9])
    #     track_label =      str(t[11])
    #     track_comment =  str(t[12])
        
    #     playlist_entry = [track_number, track_title, track_artist,\
    #                       track_album, track_year,\
    #                       timedelta(seconds=round(track_duration)),\
    #                       track_label.upper() ]

    
        
    # populate the jinja html template for the EPISODE
    # output_from_parsed_template = episode_template.render(\
    #                             show_name=show_name, episode_author=episode_author,\
    #                             episode_number=episode_number, show_url=show_url, show_rss=show_rss,\
    #                             episode_duration=timedelta(seconds=round(episode_duration)), \
    #                             episode_date=episode_date, about_show=show_short_description, \
    #                             episode_playlist=playlist_table, \
    #                             episode_image="img/cover0.png".format(episode_number))

    with open("html/episode/{0}/index.html".format(episode_number), "w") as episode_page:
        episode_page.write(output_from_parsed_template)

def create_RSS_XML_from_template():
    playlist_table = []

    set_episode_date(input_date)
    
    env = Environment(loader=FileSystemLoader('html/templates'))
    rss_template = env.get_template('show_RSS.jinja.xml')

    cursor = conn.cursor()

    cursor.execute('SELECT MAX(episode) FROM EPISODES')

    num_eps = cursor.fetchone()[0]
    num_eps = num_eps+1

    for i in range(num_eps):
        artists = []
        cursor.execute("SELECT artist FROM EPISODES WHERE episode=? ORDER BY track_number ASC", [i])
        rows = cursor.fetchall()

        for artist in rows:
            art = string=re.sub("\(.*?\)","", artist[0])
            # shorten verbose artist names such as trojnik Trojnik (Cene Resnik, Tomaž Grom, Vid Drašler)
            artist = string=re.sub("and","+",art)
            artists.append(artist)
        episode_artists.append(artists)

        dates = []
        cursor.execute("SELECT date FROM EPISODES WHERE episode=? ORDER BY track_number ASC", [i])
        d = cursor.fetchone()
        dates.append(d)

        trackdurs = []
        cursor.execute("SELECT trackdur FROM EPISODES WHERE episode=? ORDER BY track_number ASC", [i])
        td = cursor.fetchone()
        trackdurs.append(str(60*60))
        # TODO this is not what i need here - i need a final episode duraton so for now putting in a dummy value

    episodes = []
    for i in range(num_eps):      # get this from new table EPISODE_STATS number of tracks in DB
        #print(episode_artists[i])
        an_episode = dict(date="2012-02-", \
                          episode_artists=str(episode_artists[i])\
                          .strip("[").strip("]").strip("\'"), episode_number=i, \
                          episode_date=dates, episode_mp3_url=show_url+"/episode/"+str(i)+"/show.mp3",\
                          episode_duration=trackdurs)

        episodes.append(an_episode)
    # reversed order to most recent episode appears first in list
    #print(episodes)

    episodes = reversed(episodes[0:episode_number+1])
         #TODO remove single quptes not working in episode_artists
    # populate the jinja html template for the EPISODE
    #''.join(random.choice((str.upper,str.lower))(x) for x in show_name)
    output_from_parsed_template = rss_template.render(\
                                    show_name=show_name, show_url=show_url, show_rss=show_rss, \
                                    episodes=episodes, episode_author=episode_author,\
                                    episode_date=episode_date,\
                                    episode_duration=episode_duration, episode_number=episode_number, \
                                    episode_artists=str(episode_artists[episode_number])\
                                    .strip("[").strip("]").strip('\'').strip('\''), \
                                    about_show=show_short_description, \
                                    episode_image="img/cover0.jpg".format(episode_number))
    with open("html/podcast_rss.xml".format(episode_number), "w") as rss_page:
         rss_page.write(output_from_parsed_template)

def main():
    # playlist options

    if args.playlist == "new":
        print("creating new playlist >>>>> database  ")
        create_episode_playlist(conn,  episode_number)

    elif args.playlist=="keep":
        print("working with playlist from database...  ")

    elif args.playlist=="edit":
        print("edit the playlishttps://uho.rizom.si/podcast_rss.xmlt...")
        modify_playlist(conn,  episode_number, episode_duration)

    else:
        print("not creating new show....")

    if args.insert_fills == True:
        print("inserting voice fills......")

    if args.top_tail == True:
        print("inserting intro and outro......")
                
    # output options

    if args.web == True:
        print("creating html......")
        #create_animated_gif()

        create_html_episode_from_template(episode_number, episode_duration)
        create_html_homepage_from_template()
        create_RSS_XML_from_template()

    if args.art == True:
        print("creating artwork for socials ......")
        create_show_coverart(4)
        #create_animated_gif()

    if args.socials == True:
        print("creating text for socials ......")
        social_media_post()
        #create_animated_gif()


    if args.mp3 == True:
        print("creating MP3 audio output......")
        #get_intro()
        create_podcast()
    else:
        print("not creating audio output......")
    
    conn.close()

main()
