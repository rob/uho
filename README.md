# uho!

Some python magick to help with making the Uho podcast. https://uho.rizom,si

## Usage examples:

## create a database of all music files
./mk_music_library_db.py --music_directory=music --database=database/uho.db 

### create a new playlist
./mk_show.py --episode 2 --date 2024-03-11 --playlist new

### edit an existing playlist
./mk_show.py --episode 2  --playlist edit

when happy with the playlist then record the voice fills:

### record the voice fills for between the songs
each song should have a short intro description and a longer outro description

### record the intros for episode 1 on audio device 6
./recorder.py --episode 1 --sound_card 6 --fills out

### record the outros for episode 1 on audio device 0
./recorder.py -e 1 -s 0 -f out

now you have the voice fills and the playlist - time to cook up the show...

### keep the playlist and generate a website artwork and an audiofile output
./mk_show.py --episode 0 --date 2024-1-30 --playlist keep --mp3 --web --art

## unziping bandcamp into their own directory 
for i in *.zip;  do b=`basename "$i" .zip`; echo  unzip \"$i\" -d \"$b\"  ; done


## install notes
 
now using a venv so :

python3 -m venv .venv
source .venv/bin/activate

pip install tinytag scikit-image popen

sudo apt install portaudio mp3info

pip install pyaudio playsound pynput


## featured labels and artists:
https://pharmafabrik.bandcamp.com/
https://kamizdat.bandcamp.com/
https://sploh.bandcamp.com/
https://zalozba.radiostudent.si/
https://13bratarhiv.bandcamp.com/
https://klopotec.bandcamp.com/
https://hexenbrutal.bandcamp.com
https://musiclabstudio.bandcamp.com/
https://kataman.org/
https://matrixmusicarhiv.bandcamp.com/
https://moonleerecords.bandcamp.com/

https://sirom.bandcamp.com/music
https://kikiriki.bandcamp.com



