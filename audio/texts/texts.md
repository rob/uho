# texts

## intros

- welcome to uho!, 
a eclectic selection of some of the most interesting 
and innovative contemporary slovenian independent music
brought to you each week by me, your host, rob canning.


- you are listening to domaca juha - 12 tracks every week exploring the slovenian independent music scene

## fillers 
 Domaca Juha - sounds from the slovenian independent scene

## this week broadcasting from

- this week broadcasting from:
- this week once again broadcasting from:
- for the first time since last time broadcasting from:
- today we are delighted to be broadcasting from:

Zavod Rizoma's city studio located in one of the finest underground art spaces in the city of Maribor GT22 

an undisclosed location somewhere between the hinterlands of oblivion and the outskirts of absence

just this side of the event horizon of Saggitarius A star

on-location from our outside broadcast vehicle double-parked at a secret location in the boney labrynth of your ear 

beyond the singularity

from somewhere in the murky haze a few moments after the big bang but before they started playing the accordion

## this weeks mystery sound 

## on a day like today, in these strange days, its important to 
- remember what is important amoungst all of the other things, so we will go ahead now and play this next track for and you so can make up your own mind about it all -

- do the right thing in the right place at the most correct time, so I will do my bit and put this next tune on for you

## this weeks interval / frequency

## this weeks lucky number

## the following tone is not a test tone

## GPS coordinates for the crock of sonic gold 



- this is domaca juha - for this weeks track listings visit the shows website at http://rizoma.si/radio


from me to you at 300,000 meters a second this is domaca juha radio - a diverse selection of the best slovenian independent music


## Random track from a Label ..... Next, 

we roll the dice, toss the coin and spin the wheel of fortune to select.... / through caution to the wind and engage our supercomputer with the task of selecting... / crank up the semi-random number generator to assist us in selecting....

track #8  of this episode, a track from an album brough to us by Zavod Sploh's label - find out more about the label and artists at sploh.si - thats S P L O H .si Sound Performing Listening Observing Hearing - 

I hope we all enjoy this one - to find more details about this artist and other artists from this weeks episode don't forget to check the episodes webpage at http://rizom.si/dj 


## outros
variants on:

you have been listening to domaca juha - i hope you have 

enjoyed / been somehow affected by / been stimulated by/ been as excited as I have to hear 

this weeks 
selection of slovenian sonic curiosities / sounds from the slovenian independent scene
- tune in next week for more

if you like what you hear and want to dig deeper, check out the episodes webpage where you will find  tracklistings and links to the artists webpage or bandcamp where you can support them by buying thier albums and  merchendise http://rizom.si/dj



## podcast subscribe reminder

to listen to previous episodes and get notifications about new episodes subscribe to our podcast RSS feed - links on our website http://rizoma.si/dj - 

## submissions

## get in touch






